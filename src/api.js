import axios from "axios";

const URL = "http://localhost:3001";
export const askQuestion = (question) =>
  axios.post(`${URL}/question/ask`, { question });

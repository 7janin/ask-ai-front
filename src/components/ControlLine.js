import React, { useState } from "react";
import styled from "styled-components/macro";
import { Button, Input } from "semantic-ui-react";

const ControlLine = ({ isLoading, onSearch }) => {
  const [value, setValue] = useState("");

  const handleSearch = () => {
    onSearch?.(value);
    setValue("");
  };
  return (
    <Container>
      <StyledInput
        placeholder="Type your question"
        value={value}
        onChange={(e) => setValue(e.target.value)}
      />

      <Button onClick={handleSearch} loading={isLoading}>
        Search
      </Button>
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  gap: 20px;
`;

const StyledInput = styled(Input)`
  width: 300px;
`;

export default ControlLine;
